import java.util.Random;

/**
 * <h1>Classe Temperatura</h1>
 * <p>A Classe Temperatura é a classe responsavel por criar sensores reais ou simulados do tipo temperatura,
 * esta é subclasse da classe Sensor</p>
 * @author Hélder Filipe Mendonça de Medeiros Braga
 * @since 08-01-2018
 */
public class Temperatura extends Sensor
{
    protected float leitura_temperatura; //Celsius

    /**
     * <p>Construtor da classe Temperatura, cria novos sensores de Temperatura.</p>
     * @param novo_nome Nome "human readable" do sensor, exemplo "Sensor Temperatura Lagoa Zona Sul"
     * @param latitude Latitude fisica do sensor
     * @param longitude Longitude fisica do sensor
     * @param intercomm Metodo de transmição de dados, Wifi, GSM, USB, etc...
     * @since 08-01-2018
     */
    protected Temperatura(String novo_nome, int latitude, int longitude, String intercomm, float reading_temperatura)
    {
        super(novo_nome,latitude,longitude,intercomm);
        leitura_temperatura = reading_temperatura;
    }

    /**
     * <p>Metodo responsavel por obter a leitura de temperatura do arduino.</p>
     * @since 08-01-2018
     */
    public static float gerar_temperatura_random(float valor_minimo, float valor_maximo)
    {
        Random temperatura = new Random();
        float temperatura_final = temperatura.nextFloat() * (valor_maximo - valor_minimo) + valor_minimo;
        return temperatura_final;

    }

    private static void lerTemperaturaArduino()
    {
    }
}
