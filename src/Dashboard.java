/*
Autor: Hélder Filipe M. de M. Braga

TODO LIST
Arduino, Dashboard, Simulador, Sensor, Pressao, Diagrama, Relatorio, Luminosidade, Pluviosidade, Utilizador, Temperatura,
Underdog, Humidade, Controlo_rega, Gestao_ocorrencias, Premio, Movimento, Autarquia, Administrador, Controlo_trafego,
Estacionamento, Otimizacao_servico_recolha_residuos, Iluminacao_publica, Alerta

 TODO LIST LOCAL
 * Timestamp dos comentarios em italico
 * Acabar interface arduino
 * Guardar comentarios para ficheiro
 * Acabar integracao simulador no GUI
 * Acabar o forum
*/

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * <h1>Classe Dashboard</h1>
 * <p>Classe responsavel por interagir com o utilizador do software, permitindo com que este efectue as diversas funcoes requeridas
 * do programa desde que este tenha o nivel de acesso necessario, esta e composta por uma componente grafica
 * "Dashboard.form" e uma componente logica e operacional "Dashboard.java", todas as funcoes pelas quais as diversas classes
 * são responsaveis terao como seu meio de interação esta classe onde aplicavel.</p>
 * @author Hélder Filipe Mendonça de Medeiros Braga
 * @since 07-01-2018
 */
public class Dashboard {
    private JPanel painel_dashboard;
    private JSlider slider_arduino_01;
    private JComboBox combo_box_arduino_01;
    private JLabel label_arduino_02;
    private JPanel painel_arduino;
    private JButton connectButton;
    private JButton startButton;
    private JPanel painel_simulador;
    private JButton button_simulador_01;
    private JButton botao_simulador_02;
    private JComboBox combo_box_arduino_02;
    private JPanel painel_login_utilizador;
    private JPasswordField password_field_login_utilizador_01;
    private JComboBox combo_box_login_utilizador_01;
    private JButton button_login_utilizador_01;
    private JLabel label_login_utilizador_01;
    private JPanel painel_registo_utilizador;
    private JLabel label_registo_utilizador;
    private JTextField text_field_login_utilizador_01;
    private JComboBox combo_box_registo_utilizador_01;
    private JPasswordField password_field_registo_utilizador_01;
    private JButton button_registo_utilizador;
    private JTextField text_field_registo_utilizador_01;
    private JLabel label_arduino_01;
    private JPanel painel_login_efectuado;
    private JLabel label_login_efectuado_01;
    private JPanel painel_postar_comentarios;
    private JButton botao_postar_comentarios;
    private JTextField text_field_postar_comentario;
    private JLabel label_postar_comentarios;
    private JComboBox combo_box_subsistemas_e_sensores;
    private JComboBox combo_box_simulador_id_subsistemas;
    private JComboBox combo_box_simulador_temperatura;
    private JComboBox combo_box_simulador_pressao;
    private JComboBox combo_box_simulador_pluviosidade;
    private JComboBox combo_box_simulador_movimento;
    private JComboBox combo_box_simulador_luminosidade;
    private JComboBox combo_box_simulador_humidade;
    private JLabel label_simulador_humidade_nome;
    private JLabel label_simulador_nome;
    private JLabel label_simulador_latitude;
    private JLabel label_simulador_longitude;
    private JLabel label_simulador_comunicacao;
    private JLabel label_simulador_luminosidade_nome;
    private JLabel label_simulador_movimento_nome;
    private JLabel label_simulador_pluviosidade_nome;
    private JLabel label_simulador_pressao_nome;
    private JLabel label_simulador_temperatura_nome;
    private JLabel label_simulador_humidade_latitude;
    private JLabel label_simulador_humidade_longitude;
    private JLabel label_simulador_humidade_comunicacao;
    private JLabel label_simulador_leitura;
    private JLabel label_simulador_humidade_leitura;
    private JLabel label_simulador_luminosidade_latitude;
    private JLabel label_simulador_movimento_latitude;
    private JLabel label_simulador_pluviosidade_latitude;
    private JLabel label_simulador_pressao_latitude;
    private JLabel label_simulador_temperatura_latitude;
    private JLabel label_simulador_temperatura_longitude;
    private JLabel label_simulador_pressao_longitude;
    private JLabel label_simulador_pluviosidade_longitude;
    private JLabel label_simulador_movimento_longitude;
    private JLabel label_simulador_luminosidade_longitude;
    private JLabel label_simulador_luminosidade_comunicacao;
    private JLabel label_simulador_movimento_comunicacao;
    private JLabel label_simulador_pluviosidade_comunicacao;
    private JLabel label_simulador_pressao_comunicacao;
    private JLabel label_simulador_temperatura_comunicacao;
    private JLabel label_simulador_luminosidade_leitura;
    private JLabel label_simulador_movimento_leitura;
    private JLabel label_simulador_pluviosidade_leitura;
    private JLabel label_simulador_pressao_leitura;
    private JLabel label_simulador_temperatura_leitura;
    private JTextArea text_area_emitir_alerta_report;
    private JButton button_emitir_alerta;
    private JPanel painel_cidadao_emitir_alerta;
    private JTextField text_field_emitir_alerta_latitude;
    private JTextField text_field_emitir_alerta_longitude;
    private JTextField text_field_subsitema_07;
    private JTextField text_field_subsitema_06;
    private JTextField text_field_subsitema_05;
    private JTextField text_field_subsitema_04;
    private JTextField text_field_subsitema_03;
    private JTextField text_field_subsitema_02;
    private JTextField text_field_subsitema_01;
    private JLabel label_subsitema_01;
    private JLabel label_subsitema_02;
    private JLabel label_subsitema_03;
    private JLabel label_subsitema_04;
    private JLabel label_subsitema_05;
    private JLabel label_subsitema_06;
    private JLabel label_subsitema_07;
    private JTextField text_field_subsitema_08;
    private JTextField text_field_subsitema_09;
    private JTextField text_field_subsitema_10;
    private JTextField text_field_subsitema_11;
    private JTextField text_field_subsitema_12;
    private JTextField text_field_subsitema_13;
    private JTextField text_field_subsitema_14;
    private JLabel label_subsitema_08;
    private JLabel label_subsitema_09;
    private JLabel label_subsitema_10;
    private JLabel label_subsitema_11;
    private JLabel label_subsitema_12;
    private JLabel label_subsitema_13;
    private JLabel label_subsitema_14;
    private ArrayList<Administrador> listaAdministradores = new ArrayList<>();
    private ArrayList<Autarquia> listaAutarquias = new ArrayList<>();
    private ArrayList<Cidadao> listaCidadaos = new ArrayList<>();
    private static String nome_utilizador_logado = "Anonimo";
    public Dashboard() {


    }

    /**
     * <p>Permite correr a classe como executavel.</p>
     * @param args Argumentos de execução.
     */
    public static void main(String[] args)
    {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        int screen_width = gd.getDisplayMode().getWidth();
        int screen_height = gd.getDisplayMode().getHeight();


        final JFrame frameDashboard = new JFrame();
        final JFrame frameCommentarios = new JFrame("Comentarios");
        Dashboard dashboard1 = new Dashboard();
        frameDashboard.setTitle("Dashboard");
        frameDashboard.setSize(1200, 600);
        frameDashboard.getDefaultCloseOperation();
        dashboard1.combo_box_arduino_01.addItem("Humidade %");
        dashboard1.combo_box_arduino_01.addItem("Pressão P");
        dashboard1.combo_box_arduino_01.addItem("Temperatura ºC");
        dashboard1.combo_box_arduino_01.addItem("Luminosidade lx");
        dashboard1.combo_box_arduino_01.addItem("Pluviosidade cm");
        dashboard1.combo_box_arduino_01.addItem("Movimento");
        dashboard1.combo_box_arduino_01.setSelectedIndex(0);
        dashboard1.label_arduino_02.setText(Integer.toString(dashboard1.slider_arduino_01.getValue()));
        dashboard1.painel_arduino.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        dashboard1.painel_arduino.setBackground(Color.LIGHT_GRAY);//YELLOW
        dashboard1.painel_simulador.setBackground(Color.LIGHT_GRAY);//GREEN
        dashboard1.painel_simulador.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        dashboard1.painel_login_utilizador.setBackground(Color.LIGHT_GRAY);
        dashboard1.painel_login_utilizador.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        dashboard1.painel_registo_utilizador.setBackground(Color.LIGHT_GRAY);//GRAY
        dashboard1.painel_registo_utilizador.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        dashboard1.painel_cidadao_emitir_alerta.setBackground(Color.LIGHT_GRAY);//GRAY
        dashboard1.painel_cidadao_emitir_alerta.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        dashboard1.painel_simulador.setVisible(false);
        dashboard1.painel_registo_utilizador.setVisible(true);
        dashboard1.painel_arduino.setVisible(false);
        dashboard1.combo_box_login_utilizador_01.addItem("Cidadao");
        dashboard1.combo_box_login_utilizador_01.addItem("Autarquia");
        dashboard1.combo_box_login_utilizador_01.addItem("Administrador");
        dashboard1.combo_box_registo_utilizador_01.addItem("Cidadao");
        dashboard1.painel_login_efectuado.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        dashboard1.painel_login_efectuado.setBackground(Color.YELLOW);
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Arduino");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Subsistema Alerta");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Subsistema Controlo Rega");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Subsistema Controlo Trafego");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Subsistema Estacionamento");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Subsistema Gestao Ocorrencias");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Subsistema Ilumincao Publica");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Subsistema Otimizacao Servico Recolha Residuos");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Subsistema Premio");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Sensor Humidade");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Sensor Luminosidade");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Sensor Movimento");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Sensor Pluviosidade");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Sensor Pressao");
        dashboard1.combo_box_subsistemas_e_sensores.addItem("Sensor Temperatura");
        dashboard1.painel_postar_comentarios.setBackground(Color.LIGHT_GRAY);
        dashboard1.painel_postar_comentarios.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        dashboard1.painel_cidadao_emitir_alerta.setVisible(false);
        dashboard1.combo_box_simulador_humidade.setEnabled(false);
        dashboard1.combo_box_simulador_luminosidade.setEnabled(false);
        dashboard1.combo_box_simulador_movimento.setEnabled(false);
        dashboard1.combo_box_simulador_pluviosidade.setEnabled(false);
        dashboard1.combo_box_simulador_temperatura.setEnabled(false);
        dashboard1.combo_box_simulador_pressao.setEnabled(false);
        dashboard1.text_field_subsitema_01.setEnabled(false);
        dashboard1.text_field_subsitema_02.setEnabled(false);
        dashboard1.text_field_subsitema_03.setEnabled(false);
        dashboard1.text_field_subsitema_04.setEnabled(false);
        dashboard1.text_field_subsitema_05.setEnabled(false);
        dashboard1.text_field_subsitema_06.setEnabled(false);
        dashboard1.text_field_subsitema_07.setEnabled(false);
        dashboard1.text_field_subsitema_08.setEnabled(false);
        dashboard1.text_field_subsitema_09.setEnabled(false);
        dashboard1.text_field_subsitema_10.setEnabled(false);
        dashboard1.text_field_subsitema_11.setEnabled(false);
        dashboard1.text_field_subsitema_12.setEnabled(false);
        dashboard1.text_field_subsitema_13.setEnabled(false);
        dashboard1.text_field_subsitema_14.setEnabled(false);

        Administrador default_admin = new Administrador("Tostaday","tosta");//Username: Tostaday Password: tosta
        dashboard1.listaAdministradores.add(default_admin);

        frameDashboard.add(dashboard1.painel_dashboard);
        frameDashboard.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frameDashboard.setTitle("Dashboard");
        frameDashboard.setLocation(screen_width/3,screen_height/3);
        frameDashboard.setVisible(true);

        dashboard1.slider_arduino_01.addChangeListener(new ChangeListener()
        {
            @Override public void stateChanged(ChangeEvent e)
            {
                if(dashboard1.slider_arduino_01.getValueIsAdjusting())
                {
                    dashboard1.label_arduino_02.setText(Integer.toString(dashboard1.slider_arduino_01.getValue()));
                }
            }

        });
        /*
         *Atribui maximos e minimos para todos os diversos campos disponiveis na combobox do painel responsavel pela
         *conexao com o arduino
         **/
        dashboard1.combo_box_arduino_01.addActionListener(new ActionListener ()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(dashboard1.combo_box_arduino_01.getSelectedIndex() == 0) //Humidade - 0 a 100%
                {
                    dashboard1.slider_arduino_01.setMinimum(0);
                    dashboard1.slider_arduino_01.setMaximum(100);
                    dashboard1.slider_arduino_01.setValue(5);
                    dashboard1.label_arduino_02.setText(Integer.toString(dashboard1.slider_arduino_01.getValue()));
                }
                else if(dashboard1.combo_box_arduino_01.getSelectedIndex() == 1) //Pressão - Pascal
                {
                    dashboard1.slider_arduino_01.setMinimum(0);
                    dashboard1.slider_arduino_01.setMaximum(100);
                    dashboard1.slider_arduino_01.setValue(5);
                    dashboard1.label_arduino_02.setText(Integer.toString(dashboard1.slider_arduino_01.getValue()));
                }
                else if(dashboard1.combo_box_arduino_01.getSelectedIndex() == 2) //Temperatura - ºC
                {
                    dashboard1.slider_arduino_01.setMinimum(-100);
                    dashboard1.slider_arduino_01.setMaximum(300);
                    dashboard1.slider_arduino_01.setValue(20);
                    dashboard1.label_arduino_02.setText(Integer.toString(dashboard1.slider_arduino_01.getValue()));
                }
                else if(dashboard1.combo_box_arduino_01.getSelectedIndex() == 3) //Luminosidade - lx
                {
                    dashboard1.slider_arduino_01.setMinimum(0);//Noite Nublada
                    dashboard1.slider_arduino_01.setMaximum(107527); //Luz solar
                    dashboard1.slider_arduino_01.setValue(10752);//Dia com sol
                    dashboard1.label_arduino_02.setText(Integer.toString(dashboard1.slider_arduino_01.getValue()));
                }
                else if(dashboard1.combo_box_arduino_01.getSelectedIndex() == 4) //Pluviosidade - cm
                {
                    dashboard1.slider_arduino_01.setMinimum(0);
                    dashboard1.slider_arduino_01.setMaximum(50);
                    dashboard1.slider_arduino_01.setValue(2);
                    dashboard1.label_arduino_02.setText(Integer.toString(dashboard1.slider_arduino_01.getValue()));
                }
                else if(dashboard1.combo_box_arduino_01.getSelectedIndex() == 5) //Movimento
                {
                    dashboard1.slider_arduino_01.setMinimum(0);//FALSE
                    dashboard1.slider_arduino_01.setMaximum(1);//TRUE
                    dashboard1.slider_arduino_01.setValue(0);
                    dashboard1.label_arduino_02.setText(Integer.toString(dashboard1.slider_arduino_01.getValue()));
                }
            }

        });
        /*
         * Efectua o login do utilizador se o username e password existirem e forem corretos
         * */
        dashboard1.button_login_utilizador_01.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent i)
            {
                if(dashboard1.combo_box_login_utilizador_01.getSelectedItem() == "Administrador")
                {
                    if (dashboard1.listaAdministradores.size() > 0)
                    {
                        for (int j = 0; j < dashboard1.listaAdministradores.size(); j++)
                        {
                            if ((dashboard1.text_field_login_utilizador_01.getText().
                                    contentEquals(dashboard1.listaAdministradores.get(j).username))
                                    && (dashboard1.listaAdministradores.get(j).
                                    password.equals(new String(dashboard1.
                                    password_field_login_utilizador_01.getPassword()))))
                            {

                                dashboard1.label_login_efectuado_01.setText("Bem vindo " +
                                        dashboard1.combo_box_login_utilizador_01.getSelectedItem() +
                                        " " +
                                        dashboard1.text_field_login_utilizador_01.getText());
                                dashboard1.painel_arduino.setVisible(true);
                                dashboard1.painel_registo_utilizador.setVisible(true);
                                dashboard1.painel_simulador.setVisible(true);
                                dashboard1.combo_box_registo_utilizador_01.addItem("Administrador");
                                dashboard1.combo_box_registo_utilizador_01.addItem("Autarquia");
                                break;
                            }
                        }
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null,
                                "Não existe nenhum Administrador registado no sistema",
                                "ERRO", JOptionPane.ERROR_MESSAGE);
                    }
                }
                else if(dashboard1.combo_box_login_utilizador_01.getSelectedItem() == "Autarquia")
                {
                    if (dashboard1.listaAutarquias.size()>0)
                    {
                        for (int j = 0; j < dashboard1.listaAutarquias.size(); j++)
                        {
                            if ((dashboard1.text_field_login_utilizador_01.getText().
                                    contentEquals(dashboard1.listaAutarquias.get(j).username))
                                    && (dashboard1.listaAutarquias.get(j).password.equals(new String(dashboard1.
                                    password_field_login_utilizador_01.getPassword())))) {
                                dashboard1.label_login_efectuado_01.setText("Bem vindo " +
                                        dashboard1.combo_box_login_utilizador_01.getSelectedItem() +
                                        " " +
                                        dashboard1.text_field_login_utilizador_01.getText());
                                dashboard1.painel_arduino.setVisible(true);
                                dashboard1.painel_simulador.setVisible(true);
                                dashboard1.painel_registo_utilizador.setVisible(false);
                                break;
                            }
                        }
                    }
                    else
                        {
                            JOptionPane.showMessageDialog(null,
                                    "Não existe nenhuma Autarquia registada no sistema",
                                    "ERRO", JOptionPane.ERROR_MESSAGE);
                        }
                }
                else if(dashboard1.combo_box_login_utilizador_01.getSelectedItem() == "Cidadao")
                {
                    if(dashboard1.listaCidadaos.size()>0)
                    {
                    for (int j = 0; j < dashboard1.listaAdministradores.size(); j++)
                    {
                        if ((dashboard1.text_field_login_utilizador_01.getText().
                                contentEquals(dashboard1.listaCidadaos.get(j).username))
                                && (dashboard1.listaCidadaos.get(j).
                                password.equals(new String(dashboard1.
                                password_field_login_utilizador_01.getPassword()))))
                        {
                            dashboard1.label_login_efectuado_01.setText("Bem vindo " +
                                    dashboard1.combo_box_login_utilizador_01.getSelectedItem() +
                                    " " +
                                    dashboard1.text_field_login_utilizador_01.getText());
                            dashboard1.painel_arduino.setVisible(false);
                            dashboard1.painel_registo_utilizador.setVisible(true);
                            dashboard1.painel_simulador.setVisible(false);
                            dashboard1.combo_box_registo_utilizador_01.removeItem("Administrador");
                            dashboard1.combo_box_registo_utilizador_01.removeItem("Autarquia");
                            dashboard1.painel_cidadao_emitir_alerta.setVisible(true);
                            break;
                        }
                    }
                    }
                    else
                    {
                        JOptionPane.showMessageDialog(null,
                                "Não existe nenhum Cidadao registado no sistema",
                                "ERRO",JOptionPane.ERROR_MESSAGE);
                    }
                }
                else
                    {
                        JOptionPane.showMessageDialog(null,
                                "Introduza uma categoria com o nome de utilizador e password validos",
                                "ERRO",JOptionPane.ERROR_MESSAGE);
                    }
                Dashboard.nome_utilizador_logado = dashboard1.text_field_login_utilizador_01.getText();
                dashboard1.label_postar_comentarios.setText("Postar comentario como " +
                        Dashboard.nome_utilizador_logado);
                dashboard1.text_field_login_utilizador_01.setText("");
                dashboard1.password_field_login_utilizador_01.setText("");
            }
        });
        /*
         * Permite efectuar o registo de novos utilizadores
         */
        dashboard1.button_registo_utilizador.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) //Listener botao painel registo utilizador
            {
                if ((dashboard1.combo_box_registo_utilizador_01.getSelectedItem() == "Autarquia")
                        && (dashboard1.text_field_registo_utilizador_01.getText().length() > 0)
                        && (dashboard1.password_field_registo_utilizador_01.getPassword().length > 0))
                {
                    Autarquia au = new Autarquia(dashboard1.text_field_registo_utilizador_01.getText(),
                            new String( dashboard1.password_field_registo_utilizador_01.getPassword()));
                    dashboard1.listaAutarquias.add(au);
                    JOptionPane.showMessageDialog(null,
                            "Um novo utilizador do tipo Autarquia foi criado com sucesso.",
                            "Mensagem",JOptionPane.INFORMATION_MESSAGE);
                }
                else if ((dashboard1.combo_box_registo_utilizador_01.getSelectedItem() == "Administrador")
                        && (dashboard1.text_field_registo_utilizador_01.getText().length() > 0)
                        && (dashboard1.password_field_registo_utilizador_01.getPassword().length > 0))
                {
                    Administrador ad = new Administrador(dashboard1.text_field_registo_utilizador_01.getText(),
                            new String( dashboard1.password_field_registo_utilizador_01.getPassword()));
                    dashboard1.listaAdministradores.add(ad);
                    JOptionPane.showMessageDialog(null,
                            "Um novo utilizador do tipo Administrador foi criado com sucesso.",
                            "Mensagem",JOptionPane.INFORMATION_MESSAGE);
                }
                else if ((dashboard1.combo_box_registo_utilizador_01.getSelectedItem() == "Cidadao")
                        && (dashboard1.text_field_registo_utilizador_01.getText().length() > 0)
                        && (dashboard1.password_field_registo_utilizador_01.getPassword().length > 0))
                {
                    Cidadao un = new Cidadao(dashboard1.text_field_registo_utilizador_01.getText(),
                            new String(dashboard1.password_field_registo_utilizador_01.getPassword()));
                    dashboard1.listaCidadaos.add(un);
                    JOptionPane.showMessageDialog(null,
                            "Um novo utilizador do tipo Cidadao foi criado com sucesso.",
                            "Mensagem",JOptionPane.INFORMATION_MESSAGE);
                }
                else if (dashboard1.text_field_registo_utilizador_01.getText().length() < 1)
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Digite um nome para o utilizador!",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }
                else if (dashboard1.password_field_registo_utilizador_01.getPassword().length < 1)
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Digite uma password para o utilizador!",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }
                else
                {
                    JOptionPane.showMessageDialog(null,
                            "Selecione o tipo de utilizador a criar",
                            "Erro",
                            JOptionPane.ERROR_MESSAGE);
                }
                dashboard1.text_field_registo_utilizador_01.setText("");
                dashboard1.password_field_registo_utilizador_01.setText("");
            }
        });
        JPanel painel_mostrar_comentarios = new JPanel(new GridLayout(0,1));
        JScrollPane scrollPane = new JScrollPane();
        dashboard1.botao_postar_comentarios.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if (dashboard1.text_field_postar_comentario.getText().length() > 0) {
                    String texto_painel = dashboard1.text_field_postar_comentario.getText() + " - by " +
                            Dashboard.nome_utilizador_logado + " " + LocalDateTime.now();

                    JTextField textField = new JTextField(texto_painel, 0);
                    if (frameCommentarios.getComponentCount() % 2 == 0) {
                        textField.setBackground(Color.LIGHT_GRAY);
                    } else {
                        textField.setBackground(Color.WHITE);
                    }
                    textField.setEditable(false);
                    textField.setBorder(BorderFactory.createLineBorder(Color.gray));
                    painel_mostrar_comentarios.add(textField, 0);
                    painel_mostrar_comentarios.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                    scrollPane.setViewportView(painel_mostrar_comentarios);
                    frameCommentarios.add(scrollPane);
                    frameCommentarios.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
                    frameCommentarios.setSize(new Dimension(640, 480));
                    frameCommentarios.setResizable(false);
                    frameCommentarios.setLocation(screen_width / 2, screen_height / 3);
                    frameCommentarios.setVisible(true);
                }
                else
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Não pode postar comentarios em branco!",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        dashboard1.button_simulador_01.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Arduino")
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Funcionalidade em desenvolvimento",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Subsistema Alerta")
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Funcionalidade em desenvolvimento",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Subsistema Controlo Rega") {
                    dashboard1.combo_box_simulador_id_subsistemas.setEnabled(true);



                    dashboard1.combo_box_simulador_id_subsistemas.removeAllItems();
                    ArrayList<Controlo_rega> array_aux = new ArrayList<>();
                    Simulador s = new Simulador();
                    int subsistemas=1;
                    int aspersores = 1;
                    int humidade = 1;
                    int luminosidade = 1;
                    int pluviosidade = 1;
                    int minimo_humidade=0;
                    int maximo_humidade=100;
                    float minimo_pluviosidade=0.0f;
                    float maximo_pluviosidade=5.0f;
                    double minimo_luminosidade=0.0;
                    double maximo_luminosidade=107527.0;
if(dashboard1.text_field_subsitema_01.getText().length()<=0
        || dashboard1.text_field_subsitema_02.getText().length()<=0
        || dashboard1.text_field_subsitema_03.getText().length()<=0
        || dashboard1.text_field_subsitema_05.getText().length()<=0
        || dashboard1.text_field_subsitema_08.getText().length()<=0
        || dashboard1.text_field_subsitema_09.getText().length()<=0
        || dashboard1.text_field_subsitema_10.getText().length()<=0
        || dashboard1.text_field_subsitema_11.getText().length()<=0
        || dashboard1.text_field_subsitema_12.getText().length()<=0
        || dashboard1.text_field_subsitema_13.getText().length()<=0
        || dashboard1.text_field_subsitema_14.getText().length()<=0)
{
    try {
        subsistemas = Integer.parseInt(dashboard1.text_field_subsitema_01.getText());
        try {
            aspersores = Integer.parseInt(dashboard1.text_field_subsitema_08.getText());
            try {
                humidade = Integer.parseInt(dashboard1.text_field_subsitema_02.getText());
                try {
                    luminosidade = Integer.parseInt(dashboard1.text_field_subsitema_03.getText());
                    try {
                        pluviosidade = Integer.parseInt(dashboard1.text_field_subsitema_05.getText());
                        try {
                            minimo_humidade = Integer.parseInt(dashboard1.text_field_subsitema_09.getText());
                            try {
                                maximo_humidade = Integer.parseInt(dashboard1.text_field_subsitema_10.getText());
                                try {
                                    minimo_luminosidade = Double.parseDouble(dashboard1.text_field_subsitema_11.getText());
                                    try {
                                        maximo_luminosidade = Double.parseDouble(dashboard1.text_field_subsitema_12.getText());
                                        try {
                                            minimo_pluviosidade = Float.parseFloat(dashboard1.text_field_subsitema_13.getText());
                                            try {
                                                maximo_pluviosidade = Float.parseFloat(dashboard1.text_field_subsitema_14.getText());
                                            } catch (NumberFormatException p) {
                                                JOptionPane.showMessageDialog(null,
                                                        "ERRO: Introduza um valor numerico decimal positivo nao nulo para o valor de leitura de pluviosidade!",
                                                        "ERRO", JOptionPane.ERROR_MESSAGE);
                                            }
                                        } catch (NumberFormatException p) {
                                            JOptionPane.showMessageDialog(null,
                                                    "ERRO: Introduza um valor numerico decimal positivo para o valor de leitura de pluviosidade!",
                                                    "ERRO", JOptionPane.ERROR_MESSAGE);
                                        }
                                    } catch (NumberFormatException p) {
                                        JOptionPane.showMessageDialog(null,
                                                "ERRO: Introduza um valor numerico decimal positivo nao nulo para o valor de leitura de luminosidade!",
                                                "ERRO", JOptionPane.ERROR_MESSAGE);
                                    }
                                } catch (NumberFormatException p) {
                                    JOptionPane.showMessageDialog(null,
                                            "ERRO: Introduza um valor numerico decimal positivo para o valor minimo de leitura de luminosidade!",
                                            "ERRO", JOptionPane.ERROR_MESSAGE);
                                }
                            } catch (NumberFormatException p) {
                                JOptionPane.showMessageDialog(null,
                                        "ERRO: Introduza um valor numerico inteiro positivo nao nulo para o maximo de percentagem de humidade!",
                                        "ERRO", JOptionPane.ERROR_MESSAGE);
                            }
                        } catch (NumberFormatException p) {
                            JOptionPane.showMessageDialog(null,
                                    "ERRO: Introduza um valor numerico inteiro positivo para o minimo de percentagem de humidade!",
                                    "ERRO", JOptionPane.ERROR_MESSAGE);
                        }
                    } catch (NumberFormatException p) {
                        JOptionPane.showMessageDialog(null,
                                "ERRO: Introduza um valor numerico inteiro positivo nao nulo para o total de sensores de pluviosidade!",
                                "ERRO", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (NumberFormatException p) {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Introduza um valor numerico inteiro positivo nao nulo para o total de sensores de luminosidade!",
                            "ERRO", JOptionPane.ERROR_MESSAGE);

                }
            } catch (NumberFormatException p) {
                JOptionPane.showMessageDialog(null,
                        "ERRO: Introduza um valor numerico inteiro positivo nao nulo para o total de sensores de humidade!",
                        "ERRO", JOptionPane.ERROR_MESSAGE);

            }
        } catch (NumberFormatException p) {
            JOptionPane.showMessageDialog(null,
                    "ERRO: Introduza um valor numerico inteiro positivo nao nulo para o total de aspersores!",
                    "ERRO", JOptionPane.ERROR_MESSAGE);

        }
    } catch (NumberFormatException p) {
        JOptionPane.showMessageDialog(null,
                "ERRO: Introduza um valor numerico inteiro positivo nao nulo para o total de subsistemas!",
                "ERRO", JOptionPane.ERROR_MESSAGE);
    }
}
else{
                    array_aux = s.gerar_subsistemas_servico_rega(subsistemas,
                            aspersores,
                            luminosidade,
                            humidade,
                            pluviosidade);
                    for (int i = 0; i < array_aux.size(); i++) {
                        dashboard1.combo_box_simulador_id_subsistemas.addItem(array_aux.get(i).id);

                    }
                    ArrayList<Controlo_rega> sub_array_aux = array_aux;
                    dashboard1.combo_box_simulador_id_subsistemas.addActionListener(new ActionListener() {

                        @Override
                        public void actionPerformed(ActionEvent g)
                        {
                            dashboard1.combo_box_simulador_luminosidade.removeAllItems();
                            dashboard1.combo_box_simulador_humidade.removeAllItems();
                            dashboard1.combo_box_simulador_pluviosidade.removeAllItems();

                            int pointer = dashboard1.combo_box_simulador_id_subsistemas.getSelectedIndex();
                                for(int j=0;j < sub_array_aux.get(pointer).listaSensoresHumidade.size();j++)
                                {
                                    dashboard1.combo_box_simulador_humidade.addItem(sub_array_aux.get(pointer).listaSensoresHumidade.get(j));
                                }
                                for(int k=0;k < sub_array_aux.get(pointer).listaSensoresPluviosidade.size();k++)
                                {
                                    dashboard1.combo_box_simulador_pluviosidade.addItem(sub_array_aux.get(pointer).listaSensoresPluviosidade.get(k));

                                }
                                for(int l=0;l < sub_array_aux.get(pointer).listaSensoresLuminosidade.size();l++)
                                {
                                    dashboard1.combo_box_simulador_luminosidade.addItem(sub_array_aux.get(pointer).listaSensoresLuminosidade.get(l));

                                }
                            dashboard1.combo_box_simulador_humidade.addActionListener(new ActionListener() {

                                @Override
                                public void actionPerformed(ActionEvent h)
                                {
                                    int sub_pointer= dashboard1.combo_box_simulador_humidade.getSelectedIndex();
                                    dashboard1.label_simulador_humidade_longitude.setText(Integer.toString(sub_array_aux.get(pointer).listaSensoresHumidade.get(sub_pointer).GPS_longitude));
                                    dashboard1.label_simulador_humidade_latitude.setText(Integer.toString(sub_array_aux.get(pointer).listaSensoresHumidade.get(sub_pointer).GPS_latitude));
                                    dashboard1.label_simulador_humidade_comunicacao.setText(sub_array_aux.get(pointer).listaSensoresHumidade.get(sub_pointer).Interface_comunicacao);

                                    final Timer timer = new Timer(0,null);
                                    timer.stop();
                                    timer.addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e)
                                        {
                                            int valor_minimo = Integer.parseInt(dashboard1.text_field_subsitema_09.getText());
                                            int valor_maximo = Integer.parseInt(dashboard1.text_field_subsitema_10.getText());
                                            sub_array_aux.get(pointer).listaSensoresHumidade.get(sub_pointer).reading_humidade = Humidade.gerar_percentagem_humidade_random(valor_minimo, valor_maximo);
                                            dashboard1.label_simulador_humidade_leitura.setText(Integer.toString(sub_array_aux.get(pointer).listaSensoresHumidade.get(sub_pointer).reading_humidade));

                                        }
                                    });
                                    timer.setDelay(1250); //Taxa de atualização em milisegundos
                                    timer.start();

                                }
                            });

                            dashboard1.combo_box_simulador_luminosidade.addActionListener(new ActionListener() {

                                @Override
                                public void actionPerformed(ActionEvent l)
                                {
                                    int sub_pointer= dashboard1.combo_box_simulador_luminosidade.getSelectedIndex();
                                    dashboard1.label_simulador_luminosidade_longitude.setText(Integer.toString(sub_array_aux.get(pointer).listaSensoresLuminosidade.get(sub_pointer).GPS_longitude));
                                    dashboard1.label_simulador_luminosidade_latitude.setText(Integer.toString(sub_array_aux.get(pointer).listaSensoresLuminosidade.get(sub_pointer).GPS_latitude));
                                    dashboard1.label_simulador_luminosidade_comunicacao.setText(sub_array_aux.get(pointer).listaSensoresLuminosidade.get(sub_pointer).Interface_comunicacao);

                                    final Timer timer = new Timer(0,null);
                                    timer.stop();
                                    timer.addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e)
                                        {
                                            double valor_minimo = Double.parseDouble(dashboard1.text_field_subsitema_11.getText());
                                            double valor_maximo = Double.parseDouble(dashboard1.text_field_subsitema_12.getText());
                                            sub_array_aux.get(pointer).listaSensoresLuminosidade.get(sub_pointer).leitura_lux = Luminosidade.gerar_lux_random(valor_minimo, valor_maximo);
                                            dashboard1.label_simulador_luminosidade_leitura.setText(Double.toString(sub_array_aux.get(pointer).listaSensoresLuminosidade.get(sub_pointer).leitura_lux));
                                        }
                                    });
                                    timer.setDelay(1250); //Taxa de atualização em milisegundos
                                    timer.start();
                                }
                            });
                            dashboard1.combo_box_simulador_pluviosidade.addActionListener(new ActionListener() {

                                @Override
                                public void actionPerformed(ActionEvent p)
                                {
                                    int sub_pointer= dashboard1.combo_box_simulador_pluviosidade.getSelectedIndex();
                                    dashboard1.label_simulador_pluviosidade_longitude.setText(Integer.toString(sub_array_aux.get(pointer).listaSensoresPluviosidade.get(sub_pointer).GPS_longitude));
                                    dashboard1.label_simulador_pluviosidade_latitude.setText(Integer.toString(sub_array_aux.get(pointer).listaSensoresPluviosidade.get(sub_pointer).GPS_latitude));
                                    dashboard1.label_simulador_pluviosidade_comunicacao.setText(sub_array_aux.get(pointer).listaSensoresPluviosidade.get(sub_pointer).Interface_comunicacao);

                                    final Timer timer = new Timer(0,null);
                                    timer.stop();
                                    timer.addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent e)
                                        {
                                            float valor_minimo = Float.parseFloat(dashboard1.text_field_subsitema_13.getText());
                                            float valor_maximo = Float.parseFloat(dashboard1.text_field_subsitema_14.getText());
                                            sub_array_aux.get(pointer).listaSensoresPluviosidade.get(sub_pointer).leitura_cm_pluviosidade = Pluviosidade.gerar_cm_pluviosidade_random(valor_minimo, valor_maximo);
                                            dashboard1.label_simulador_pluviosidade_leitura.setText(Float.toString(sub_array_aux.get(pointer).listaSensoresPluviosidade.get(sub_pointer).leitura_cm_pluviosidade));

                                        }
                                    });
                                    timer.setDelay(1250); //Taxa de atualização em milisegundos
                                    timer.start();
                                }
                            });
                        }
                    });
                }
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Subsistema Controlo Trafego")
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Funcionalidade em desenvolvimento",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Subsistema Estacionamento")
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Funcionalidade em desenvolvimento",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Subsistema Gestão Ocorrencias")
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Funcionalidade em desenvolvimento",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Subsitema Iluminacao Publica")
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Funcionalidade em desenvolvimento",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Subsistema Otimizacao Servico Recolha Residuos")
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Funcionalidade em desenvolvimento",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Subsistema Premio")
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Funcionalidade em desenvolvimento",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Sensor Humidade")
                {


                    //dashboard1.combo_box_simulador_humidade.removeAllItems();
                    Simulador s = new Simulador();

                    Humidade h = s.simularSensorHumidade();
                    dashboard1.label_simulador_humidade_nome.setText(h.nome);
                    dashboard1.label_simulador_humidade_latitude.setText(Integer.toString(h.GPS_latitude));
                    dashboard1.label_simulador_humidade_longitude.setText(Integer.toString(h.GPS_longitude));
                    dashboard1.label_simulador_humidade_comunicacao.setText(h.Interface_comunicacao);

                    final Timer timer = new Timer(0,null);
                    timer.restart();
                    timer.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e)
                        {
                            h.reading_humidade = Humidade.gerar_percentagem_humidade_random(0, 100);
                            dashboard1.label_simulador_humidade_leitura.setText(Integer.toString(h.reading_humidade));
                            timer.setDelay(1250); //Taxa de atualização em milisegundos
                        }

                    });
                    //timer.stop();
                    timer.start();

                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Sensor Luminosidade")
                {



                    Simulador s = new Simulador();

                    Luminosidade l = s.simularSensorLuminosidade();
                    dashboard1.label_simulador_luminosidade_nome.setText(l.nome);
                    dashboard1.label_simulador_luminosidade_latitude.setText(Integer.toString(l.GPS_latitude));
                    dashboard1.label_simulador_luminosidade_longitude.setText(Integer.toString(l.GPS_longitude));
                    dashboard1.label_simulador_luminosidade_comunicacao.setText(l.Interface_comunicacao);

                    final Timer timer = new Timer(0,null);
                    timer.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e)
                        {
                            setLeituraLuminosidadeLabel();
                        }
                        public void setLeituraLuminosidadeLabel()
                        {
                            l.leitura_lux = Luminosidade.gerar_lux_random(0.00001, 107527);
                            dashboard1.label_simulador_luminosidade_leitura.setText(Double.toString(l.leitura_lux));
                            timer.setDelay(1250); //Taxa de atualização em milisegundos
                        }
                    });
                    //timer.stop();
                    timer.start();
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Sensor Movimento")
                {


                    Simulador s = new Simulador();

                    Movimento m = s.simularSensorMovimento();
                    dashboard1.label_simulador_movimento_nome.setText(m.nome);
                    dashboard1.label_simulador_movimento_latitude.setText(Integer.toString(m.GPS_latitude));
                    dashboard1.label_simulador_movimento_longitude.setText(Integer.toString(m.GPS_longitude));
                    dashboard1.label_simulador_movimento_comunicacao.setText(m.Interface_comunicacao);

                    final Timer timer = new Timer(0,null);
                    timer.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e)
                        {
                            setLeituraMovimentoLabel();
                        }
                        public void setLeituraMovimentoLabel()
                        {
                            m.leitura_movimento = Movimento.gerar_movimento_random();
                            dashboard1.label_simulador_movimento_leitura.setText(Boolean.toString(m.leitura_movimento));
                            timer.setDelay(1250); //Taxa de atualização em milisegundos
                        }
                    });
                    //timer.stop();
                    timer.start();
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Sensor Pluviosidade")
                {


                    Simulador s = new Simulador();

                    Pluviosidade p = s.simularSensorPluviosidade();
                    dashboard1.label_simulador_pluviosidade_nome.setText(p.nome);
                    dashboard1.label_simulador_pluviosidade_latitude.setText(Integer.toString(p.GPS_latitude));
                    dashboard1.label_simulador_pluviosidade_longitude.setText(Integer.toString(p.GPS_longitude));
                    dashboard1.label_simulador_pluviosidade_comunicacao.setText(p.Interface_comunicacao);

                    final Timer timer = new Timer(0,null);
                    timer.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e)
                        {
                            setLeituraPluviosidadeLabel();
                        }
                        public void setLeituraPluviosidadeLabel()
                        {
                            p.leitura_cm_pluviosidade = Pluviosidade.gerar_cm_pluviosidade_random(0.0f,25.0f);
                            dashboard1.label_simulador_pluviosidade_leitura.setText(Float.toString(p.leitura_cm_pluviosidade));
                            timer.setDelay(1250); //Taxa de atualização em milisegundos
                        }
                    });
                    //timer.stop();
                    timer.start();
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Sensor Pressao")
                {



                    Simulador s = new Simulador();

                    Pressao pr = s.simularSensorPressao();
                    dashboard1.label_simulador_pressao_nome.setText(pr.nome);
                    dashboard1.label_simulador_pressao_latitude.setText(Integer.toString(pr.GPS_latitude));
                    dashboard1.label_simulador_pressao_longitude.setText(Integer.toString(pr.GPS_longitude));
                    dashboard1.label_simulador_pressao_comunicacao.setText(pr.Interface_comunicacao);

                    final Timer timer = new Timer(0,null);
                    timer.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e)
                        {
                            setLeituraPressaoLabel();
                        }
                        public void setLeituraPressaoLabel()
                        {
                            pr.leitura_pressao = Pressao.gerar_pressao_random(0.0f,25.0f);
                            dashboard1.label_simulador_pressao_leitura.setText(Float.toString(pr.leitura_pressao));
                            timer.setDelay(1250); //Taxa de atualização em milisegundos
                        }
                    });
                    //timer.stop();
                    timer.start();
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedItem() == "Sensor Temperatura")
                {

                    Simulador s = new Simulador();

                    Temperatura t = s.simularSensorTemperatura();
                    dashboard1.label_simulador_temperatura_nome.setText(t.nome);
                    dashboard1.label_simulador_temperatura_latitude.setText(Integer.toString(t.GPS_latitude));
                    dashboard1.label_simulador_temperatura_longitude.setText(Integer.toString(t.GPS_longitude));
                    dashboard1.label_simulador_temperatura_comunicacao.setText(t.Interface_comunicacao);

                    final Timer timer = new Timer(0,null);
                    timer.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e)
                        {
                            setLeituraTemperaturaLabel();
                        }
                        public void setLeituraTemperaturaLabel()
                        {
                            t.leitura_temperatura = Temperatura.gerar_temperatura_random(-60.0f,60.0f);
                            dashboard1.label_simulador_temperatura_leitura.setText(Float.toString(t.leitura_temperatura));
                            timer.setDelay(1250); //Taxa de atualização em milisegundos
                        }
                    });
                    //timer.stop();
                    timer.start();
                }

                else
                {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Selecione um item da lista",
                            "ERRO",JOptionPane.ERROR_MESSAGE);
                }

            }
        });





        dashboard1.combo_box_subsistemas_e_sensores.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 0)//Arduino
                {
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 1)//Subsistema Alerta
                {

                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 2)//Sensor Controlo Rega
                {
                    dashboard1.combo_box_simulador_humidade.setEnabled(true);
                    dashboard1.combo_box_simulador_luminosidade.setEnabled(true);
                    dashboard1.combo_box_simulador_movimento.setEnabled(false);
                    dashboard1.combo_box_simulador_pluviosidade.setEnabled(true);
                    dashboard1.combo_box_simulador_pressao.setEnabled(false);
                    dashboard1.combo_box_simulador_temperatura.setEnabled(false);
                    dashboard1.combo_box_simulador_id_subsistemas.setEnabled(false);
                    dashboard1.text_field_subsitema_01.setEnabled(true); //Subsistemas
                    dashboard1.text_field_subsitema_02.setEnabled(true); //Humidade
                    dashboard1.text_field_subsitema_03.setEnabled(true); //Luminosidade
                    dashboard1.text_field_subsitema_04.setEnabled(false); //Pluviosidade
                    dashboard1.text_field_subsitema_05.setEnabled(true);
                    dashboard1.text_field_subsitema_06.setEnabled(false);
                    dashboard1.text_field_subsitema_07.setEnabled(false);
                    dashboard1.text_field_subsitema_08.setEnabled(true); //Aspersores
                    dashboard1.label_subsitema_08.setText("Aspersores");
                    dashboard1.text_field_subsitema_09.setEnabled(true);
                    dashboard1.label_subsitema_09.setText("Minimo % humidade");
                    dashboard1.text_field_subsitema_10.setEnabled(true);
                    dashboard1.label_subsitema_10.setText("Maximo % humidade");
                    dashboard1.text_field_subsitema_11.setEnabled(true);
                    dashboard1.label_subsitema_11.setText("Minimo lux luminosidade");
                    dashboard1.text_field_subsitema_12.setEnabled(true);
                    dashboard1.label_subsitema_12.setText("Maximo lux luminosidade");
                    dashboard1.text_field_subsitema_13.setEnabled(true);
                    dashboard1.label_subsitema_13.setText("Minimo cm pluviosidade");
                    dashboard1.text_field_subsitema_14.setEnabled(true);
                    dashboard1.label_subsitema_14.setText("Maximo cm pluviosidade");
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 3)//Sensor Controlo Trafego
                {

                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 4)//Subsistema Estacionamento
                {

                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 5)//Subsistema Gestao Ocorrencias
                {

                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 6)//Subsistema Iluminacao Publica
                {

                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 7)//Subsistema Otimizacao Servico Recolha Residuos
                {

                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 8)//Subsistema Premio
                {

                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 9)//Sensor Humidade
                {
                    dashboard1.combo_box_simulador_humidade.setEnabled(false);
                    dashboard1.combo_box_simulador_luminosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_movimento.setEnabled(false);
                    dashboard1.combo_box_simulador_pluviosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_pressao.setEnabled(false);
                    dashboard1.combo_box_simulador_temperatura.setEnabled(false);
                    dashboard1.combo_box_simulador_id_subsistemas.setEnabled(false);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 10)//Sensor Luminosidade
                {
                    dashboard1.combo_box_simulador_humidade.setEnabled(false);
                    dashboard1.combo_box_simulador_luminosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_movimento.setEnabled(false);
                    dashboard1.combo_box_simulador_pluviosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_pressao.setEnabled(false);
                    dashboard1.combo_box_simulador_temperatura.setEnabled(false);
                    dashboard1.combo_box_simulador_id_subsistemas.setEnabled(false);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 11)//Sensor Movimento
                {
                    dashboard1.combo_box_simulador_humidade.setEnabled(false);
                    dashboard1.combo_box_simulador_luminosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_movimento.setEnabled(false);
                    dashboard1.combo_box_simulador_pluviosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_pressao.setEnabled(false);
                    dashboard1.combo_box_simulador_temperatura.setEnabled(false);
                    dashboard1.combo_box_simulador_id_subsistemas.setEnabled(false);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 12)//Sensor Pluviosidade
                {
                    dashboard1.combo_box_simulador_humidade.setEnabled(false);
                    dashboard1.combo_box_simulador_luminosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_movimento.setEnabled(false);
                    dashboard1.combo_box_simulador_pluviosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_pressao.setEnabled(false);
                    dashboard1.combo_box_simulador_temperatura.setEnabled(false);
                    dashboard1.combo_box_simulador_id_subsistemas.setEnabled(false);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 13)//Sensor Pressao
                {
                    dashboard1.combo_box_simulador_humidade.setEnabled(false);
                    dashboard1.combo_box_simulador_luminosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_movimento.setEnabled(false);
                    dashboard1.combo_box_simulador_pluviosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_pressao.setEnabled(false);
                    dashboard1.combo_box_simulador_temperatura.setEnabled(false);
                    dashboard1.combo_box_simulador_id_subsistemas.setEnabled(false);
                }
                else if(dashboard1.combo_box_subsistemas_e_sensores.getSelectedIndex() == 14)//Sensor Temperatura
                {
                    dashboard1.combo_box_simulador_humidade.setEnabled(false);
                    dashboard1.combo_box_simulador_luminosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_movimento.setEnabled(false);
                    dashboard1.combo_box_simulador_pluviosidade.setEnabled(false);
                    dashboard1.combo_box_simulador_pressao.setEnabled(false);
                    dashboard1.combo_box_simulador_temperatura.setEnabled(false);
                    dashboard1.combo_box_simulador_id_subsistemas.setEnabled(false);
                }

            }

        });

        dashboard1.button_emitir_alerta.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) {
                Cidadao c = dashboard1.listaCidadaos.get(0);
                for (int i = 0; i < dashboard1.listaCidadaos.size(); i++) {
                    if (dashboard1.label_login_efectuado_01.getText() == dashboard1.listaCidadaos.get(i).username) {
                        c = dashboard1.listaCidadaos.get(i);
                    }
                }
                if (dashboard1.text_field_emitir_alerta_latitude.getText().length() > 0 &&
                        dashboard1.text_field_emitir_alerta_longitude.getText().length() > 0 &&
                        dashboard1.text_area_emitir_alerta_report.getText().length() > 0)
                {
                    int lat;
                    int lon;
                    try
                    {
                        lat = Integer.parseInt(dashboard1.text_field_emitir_alerta_latitude.getText());
                        try
                        {
                            lon = Integer.parseInt(dashboard1.text_field_emitir_alerta_longitude.getText());


                            Alerta a = new Alerta(lat,
                                    lon,
                                    dashboard1.text_area_emitir_alerta_report.getText(),
                                    c);

                            JOptionPane.showMessageDialog(null,
                                    "Foi criado um novo alerta com os seguintes dados: " +
                                            "\nLatitude: " + Integer.toString(a.GPS_latitude) +
                                            "\nLongitude: " + Integer.toString(a.GPS_longitude) +
                                            "\nRelatorio: " + a.mensagem +
                                            "\nCidadão: " + a.cidadao.username +
                                            "\n\n Obrigado pela sua colaboração, a sua submicao sera analizada por " +
                                            "um dos nossos especialistas.\nSinceramente, A Autarquia.",
                                    "Informação", JOptionPane.INFORMATION_MESSAGE);

                        }
                        catch (NumberFormatException f)
                        {

                            JOptionPane.showMessageDialog(null,
                                    "ERRO: Introduza um valor numerico inteiro para a longitude!",
                                    "ERRO", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    catch (NumberFormatException f)
                    {

                        JOptionPane.showMessageDialog(null,
                                "ERRO: Introduza um valor numerico inteiro para a latitude!",
                                "ERRO", JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null,
                            "ERRO: Preencha todos os campos requisitados",
                            "ERRO", JOptionPane.ERROR_MESSAGE);
                }
            }
            });
        }
    }