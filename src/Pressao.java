import sun.dc.pr.PRError;

import java.util.Random;

public class Pressao extends Sensor
{
    //1 ATM = 14.22 PSI = 98043.271419 N/m2
    protected float leitura_pressao;//Pascal (Newton por metro quadrado N/m2)
    Pressao(String novo_nome, int latitude, int longitude, String intercomm, float reading_pressao)
    {
        super(novo_nome,latitude,longitude,intercomm);
        leitura_pressao = reading_pressao;
    }
    public static float gerar_pressao_random(float valor_minimo, float valor_maximo)
    {
        if(valor_minimo < 0){valor_minimo = 0;}
        Random pressao = new Random();
        float pressao_final = pressao.nextFloat() * (valor_maximo - valor_minimo) + valor_minimo;
        return pressao_final;
    }
}
