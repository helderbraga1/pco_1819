/**
 * <h1>Classe Utilizador</h1>
 * <p>A Classe Utilizador é a superclasse das classes Administrador, Autarquia e Cidadao,
 * esta não é usada para a criação de objectos mas apenas para passagem de atributos para
 * as suas subclasses atraves de herança.</p>
 * @see Administrador
 * @see Autarquia
 * @see Cidadao
 * @author Hélder Filipe Mendonça de Medeiros Braga
 * @since 07-01-2018
 */
public class Utilizador
{
    protected String username;
    protected String password;
    /**
     * <p>Classe construtora da classe Utilizador, utilizada pelas subclasses da mesma.</p>
     * @param user contem o nome do utilizador a criar
     * @param pass contem a password do utilizador a criar
     * @since 07-01-2018
     */
    protected Utilizador(String user, String pass)
    {
        username = user;
        password = pass;
    }
}
