import java.util.Random;

public class Pluviosidade extends Sensor
{
    protected float leitura_cm_pluviosidade;
    Pluviosidade(String novo_nome,int latitude,int longitude, String intercomm, float leitura_pluviosidade)
    {
        super(novo_nome,latitude,longitude,intercomm);
        leitura_cm_pluviosidade = leitura_pluviosidade;
    }
    public static float gerar_cm_pluviosidade_random(float valor_minimo, float valor_maximo)//0 a n em cm
    {
        if(valor_minimo < 0){valor_minimo = 0;}
        Random cm = new Random();
        float cm_final = cm.nextFloat() * (valor_maximo - valor_minimo) + valor_minimo;
        return cm_final;
    }
}
