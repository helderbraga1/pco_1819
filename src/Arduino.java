import com.fazecast.jSerialComm.*;
import java.util.Scanner;
/**
 * <h1>Classe Arduino</h1>
 * <p>Classe responsavel por efectuar a conexão remota com um arduino para obter dados de sensores ligados ao mesmo,
 * estes sensores podem ser de humidade, temperatura, pluviosidade entre outros, a conexão é feita atraves de USB mas
 * utilizando o protocolo de porta paralela.</p>
 * @author Hélder Filipe Mendonça de Medeiros Braga
 * @since 08-01-2018
 */
public class Arduino
{

    static SerialPort choosenPort;
    public static SerialPort[] GetPorts()
    {
        SerialPort[] portas = SerialPort.getCommPorts();
        return portas;
    }
    public static void Listener()
    {

    }
}
