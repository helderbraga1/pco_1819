import java.util.ArrayList;

public class Controlo_rega extends Subsistema
{

    private int total_aspersores_rega;
    private boolean estado_aspersores; //FALSE sem estar a fazer rega, TRUE a fazer rega
    private ArrayList<Object> listaTodosSensores =new ArrayList<Object>();
    protected ArrayList<Luminosidade> listaSensoresLuminosidade = new ArrayList<Luminosidade>();
    protected ArrayList<Humidade> listaSensoresHumidade = new ArrayList<Humidade>();
    protected ArrayList<Pluviosidade> listaSensoresPluviosidade = new ArrayList<Pluviosidade>();

    Controlo_rega(int latitude, int longitude, int total_aspersores, int total_sensores_luminosidade, int total_sensores_humidade, int total_sensores_pluviosidade)
    {
        super(latitude, longitude);
        estado_aspersores = false;
        total_aspersores_rega = total_aspersores;
        for(int i=0;i<total_sensores_luminosidade;i++)
        {
            Luminosidade l = new Luminosidade("",latitude,longitude,"NULL",Luminosidade.gerar_lux_random(0,107527));
            listaSensoresLuminosidade.add(l);
        }
        listaTodosSensores.add(listaSensoresLuminosidade);
        for(int j=0;j<total_sensores_humidade;j++)
        {
            Humidade h = new Humidade("",latitude,longitude,"NULL",Humidade.gerar_percentagem_humidade_random(0,100));
            listaSensoresHumidade.add(h);
        }
        listaTodosSensores.add(listaSensoresHumidade);
        for(int k=0;k<total_sensores_pluviosidade;k++)
        {
            Pluviosidade p = new Pluviosidade("",latitude,longitude,"NULL",Pluviosidade.gerar_cm_pluviosidade_random(0,10));
            listaSensoresPluviosidade.add(p);
        }
        listaTodosSensores.add(listaSensoresPluviosidade);


    }
}