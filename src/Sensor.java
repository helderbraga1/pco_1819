import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * <h1>Classe Sensor</h1>
 * <p>Classe responsaver por fazer a inicialização de grande parte dos parametros de todos os sensores existentes,
 * esta é superclasse dos sensores de Humidade, Temperatura,etc...</p>
 * @author Hélder Filipe Mendonça de Medeiros Braga
 * @since 08-01-2018
 */
public class Sensor
{
    protected static int id=0;
    protected String nome;
    protected int GPS_latitude;
    protected int GPS_longitude;
    protected String Interface_comunicacao; //Wifi, GPS, GSM
    private Path SENSORES_PATH = Paths.get("/Logs/Sensores");
    private File f = new File(SENSORES_PATH+Integer.toString(id)+".txt");

    /**
     * <p>Construtor da classe Sensor, serve como base para todos os sensores.</p>
     * @param novo_nome Nome "human readable" do sensor, exemplo "Pinhal da Paz, Zona Oeste"
     * @param latitude Latitude fisica do sensor
     * @param longitude Longitude fisica do sensor
     * @param intercomm Metodo de transmição de dados, WIFI, GSM, USB, etc...
     */
    protected Sensor(String novo_nome, int latitude, int longitude, String intercomm)
    {
        id = id++;
        nome = novo_nome;
        GPS_latitude = latitude;
        GPS_longitude = longitude;
        Interface_comunicacao = intercomm;
    }
}
