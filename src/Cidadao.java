/**
 * <h1>Classe Cidadao</h1>
 * <p>Classe responsavel por criar utilizadores do tipo Cidadão, estes são os utilizadores com o nivel de
 * previlegio mais baixo existente, estes podem enviar alertas para o sistema da smart city que podem depois
 * ver verificados como verdadeiros e geridos conforme o seu tipo de ocorrencia.</p>
 * @author Hélder Filipe Mendonça de Medeiros Braga
 * @since 08-01-2018
 */
public class Cidadao extends Utilizador
{
    /**
     * <p>Construtor da Classe Cidadão, utilizado para criar um novo utilizador do tipo cidadao, utiliza o construtor da
     * superclasse como base.</p>
     * @param u Nome de utilizador do cidadao a criar
     * @param p Password do utilizador do cidadao a criar
     * @since 08-01-2018
     */
    protected Cidadao(String u, String p)
    {
        super(u,p); //Nota: Tem de ser a primeira linha do construtor da subclasse
    }
}