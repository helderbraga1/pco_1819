import java.util.Random;

public class Movimento extends Sensor
{
    protected boolean leitura_movimento;
    Movimento(String novo_nome, int latitude, int longitude, String intercomm, boolean estado_movimento)
    {
        super(novo_nome,latitude,longitude,intercomm);
        leitura_movimento = estado_movimento;
    }
    public static boolean gerar_movimento_random()//Noite nublada: 0.00001 lux Luz solar: 107527 lux
    {
        Random mov = new Random();
        boolean mov_final = mov.nextBoolean();
        return mov_final;
    }
}
