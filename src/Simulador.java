import java.util.ArrayList;
import java.util.Random;

/**
 * <h1>Classe Simulador</h1>
 * A classe simulador é a classe responsavel por efectuar a simulação de situações quer com dados reais quer com dados
 * ficticios, este serve de base para o painel simulador existente no form Dashboard.
 * @author Hélder Filipe Mendonça de Medeiros Braga
 * @since 08-01-2018
 */
public class Simulador
{
    private ArrayList<Humidade> listaSensoresHumidade = new ArrayList<>();
    private ArrayList<Temperatura> listaSensoresTemperatura = new ArrayList<>();
    private ArrayList<Luminosidade> listaSensoresLuminosidade = new ArrayList<>();
    private ArrayList<Movimento> listaSensoresMovimento = new ArrayList<>();
    private ArrayList<Pluviosidade> listaSensoresPluviosidade = new ArrayList<>();
    private ArrayList<Pressao> listaSensoresPressao = new ArrayList<>();
    private ArrayList<Premio> listaSubsistemasPremio = new ArrayList<>();
    private ArrayList<Otimizacao_servico_recolha_residuos> listaSubsistemasOtimizacaoServicoRecolhaResiduos = new ArrayList<>();
    private ArrayList<Iluminacao_publica> listaSubsistemasIluminacaoPublica = new ArrayList<>();
    private ArrayList<Gestao_ocorrencias> listaSubsistemasGestaoOcorrencias = new ArrayList<>();
    private ArrayList<Estacionamento> listaSubsistemasEstacionamento = new ArrayList<>();
    private ArrayList<Controlo_trafego> listaSubsistemasControloTrafego = new ArrayList<>();
    private ArrayList<Controlo_rega> listaSubsistemasControloRega = new ArrayList<>();
    private void gerar_subsistemas_Otimizacao_servico_recolha_residuos(int total_subsistemas_gerados)
    {
        for(int i=0;i<total_subsistemas_gerados;i++)
        {
            Random random = new Random();
            int latitude =  random.nextInt(90 + 1);
            int longitude = random.nextInt(180  + 1);
            Otimizacao_servico_recolha_residuos osrr = new Otimizacao_servico_recolha_residuos(latitude,longitude);
            listaSubsistemasOtimizacaoServicoRecolhaResiduos.add(osrr);
        }
    }
    private void gerar_subsistemas_iluminacao_publica(int total_subsistemas_gerados)
    {
        for(int i=0;i<total_subsistemas_gerados;i++)
        {
            Random random = new Random();
            int latitude =  random.nextInt(90 + 1);
            int longitude = random.nextInt(180 + 1);
            Iluminacao_publica ip = new Iluminacao_publica(Simulador.gerar_latitude(),Simulador.gerar_longitude());
            listaSubsistemasIluminacaoPublica.add(ip);
        }
    }

    private static String gerar_intercomm()
    {
        ArrayList<String> comms = new ArrayList<String>();
        comms.add("USB");
        comms.add("Paralela");
        comms.add("WiFi");
        comms.add("GSM");
        comms.add("LTE");
        comms.add("3G");
        comms.add("4G");
        comms.add("Ethernet");
        Random k = new Random();
        int i = k.nextInt(comms.size()) + 0;
        String intercomm = comms.get(i);
        return intercomm;
    }
    public ArrayList<Controlo_rega> gerar_subsistemas_servico_rega(int total_subsistemas_gerados,
                                                int total_aspersores_por_subsistema,
                                                int total_sensores_luminosidade_por_subsistema,
                                                int total_sensores_humidade_por_subsistema,
                                                int total_sensores_pluviosidade_por_subsistema)
    {
        ArrayList<Controlo_rega> array_aux = new ArrayList<>();
        for(int i=0;i<total_subsistemas_gerados;i++)
        {
            Controlo_rega cr = new Controlo_rega(Simulador.gerar_latitude(),Simulador.gerar_longitude(),
                                                total_aspersores_por_subsistema,total_sensores_luminosidade_por_subsistema,
                                                total_sensores_humidade_por_subsistema,total_sensores_pluviosidade_por_subsistema);
            array_aux.add(cr);
            listaSubsistemasControloRega.add(cr);
        }
        return array_aux;
    }


    private static int gerar_latitude()
    {
        Random random = new Random();
        int latitude =  random.nextInt(90 + 1);
        return latitude;
    }
    private static int gerar_longitude()
    {
        Random random = new Random();
        int longitude = random.nextInt(180 + 1);
        return longitude;
    }/*
    private void gerar_sensores()
    {

    }
    private void gerar_sensores()
    {

    }
    private void gerar_sensores()
    {

    }
    private void gerar_sensores()
    {

    }
    private void gerar_sensores()
    {

    }

    */

    public Humidade simularSensorHumidade()
    {
        Humidade h = new Humidade("Sensor humidade simulado",
                gerar_latitude(),
                gerar_longitude(),
                gerar_intercomm(),
                Humidade.gerar_percentagem_humidade_random(0,100));
        return h;
    }
    public Luminosidade simularSensorLuminosidade()
    {
        Luminosidade l = new Luminosidade("Sensor luminosidade simulado",
                gerar_latitude(),
                gerar_longitude(),
                gerar_intercomm(),
                Luminosidade.gerar_lux_random(0.00001, 107527));
        return l;

    }
    public Movimento simularSensorMovimento()
    {
        Movimento m = new Movimento("Sensor movimento simulado",
                gerar_latitude(),
                gerar_longitude(),
                gerar_intercomm(),
                Movimento.gerar_movimento_random());
        return m;

    }
    public Pluviosidade simularSensorPluviosidade()
    {
        Pluviosidade p = new Pluviosidade("Sensor pluviosidade simulado",
                gerar_latitude(),
                gerar_longitude(),
                gerar_intercomm(),
                Pluviosidade.gerar_cm_pluviosidade_random(0.0f,25.0f));
        return p;

    }
    public Pressao simularSensorPressao()
    {

        Pressao pr = new Pressao("Sensor pressao simulado",
                gerar_latitude(),
                gerar_longitude(),
                gerar_intercomm(),
                Pressao.gerar_pressao_random(0.0f,25.0f));
        return pr;

    }
    public Temperatura simularSensorTemperatura()
    {

        Temperatura t = new Temperatura("Sensor temperatura simulado",
                gerar_latitude(),
                gerar_longitude(),
                gerar_intercomm(),
                Temperatura.gerar_temperatura_random(-60.0f,60.0f));
        return t;

    }
    private static void simularSubsistemaRega()
    {

    }
    private static void simularSubsistemaTrafico()
    {

    }
}
