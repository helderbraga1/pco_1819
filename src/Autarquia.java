/**
 * <h1>Classe Autarquia</h1>
 * <p>Classe responsavel por criar utilizadores do tipo Autarquia, esta é subclasse da classe Utilizador
 * herdando os atributos username e password da classe mãe.</p>
 * @author Hélder Filipe Mendonça de Medeiros Braga
 * @since 07-01-2018
 */
public class Autarquia extends Utilizador
{
    /**
     * <p>Classe constutora da Classe Autarquia, utiliza o construtor da superclasse Utilizador.</p>
     * @param u nome do utilizador a criar
     * @param p password do utilizador a criar
     * @see Utilizador#Utilizador(String, String)
     * @since 07-01-2018
     */
    protected Autarquia(String u, String p)
    {
     super(u,p); //Nota: Tem de ser a primeira linha do construtor da subclasse
    }
 }