/**
 * <h1>Classe Subsistema</h1>
 * <p>Classe responsavel por efectuar a gestao de sensores e subsistemas existentes.</p>
 * @author Hélder Filipe Mendonça de Medeiros Braga
 * @since 08-01-2018
 */
public class Subsistema
{
    protected static int contador;
    protected int id;
    protected int GPS_latitude;
    protected int GPS_longitude;

    /**
     * <p>Construtor da Classe Subsistema, serve como ccnstrutor base para grande parte dos subsistemas existentes</p>
     * @param latitude Latitude do subsistema criado.
     * @param longitude Longitude do subsistema criado.
     * @since 08-01-2018
     */
    protected Subsistema(int latitude,int longitude)
    {
        id=contador;
        contador++;
        GPS_latitude = latitude;
        GPS_longitude = longitude;
    }
}
