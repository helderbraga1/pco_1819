import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * <h1>Classe Humidade</h1>
 * <p>A Classe Humidade é a classe responsavel por criar sensores do tipo humidade quer para casos ficticios quer para
 * casos reais, esta é subclasse da classe Sensor e utiliza alguns dos seus construtores</p>
 * @author Hélder Filipe mendonça de Medeiros Braga
 * @since 08-01-2018
 */
public class Humidade extends Sensor
{
    protected int reading_humidade; //0 a 100 %

    /**
     * <p>Função construtora da Classe Humidade, esta utiliza o construtor da classe mãe como auxilio para que seja
     * criado um novo sensor, ficticio ou real, do tipo Humidade</p>
     * @param novo_nome Contem o nome dado ao sensor, exemplo, "Sensor Humidade Jardim Antonio Borges Zona Norte"
     * @param latitude Especifica a latitude fisica de onse se encontra o sensor
     * @param longitude Especifica a longitude fisica de onde se encontra o sensor.
     * @param intercomm Especifica a forma de como o sensor reporta os dados, Wifi, USB, Ethernet, rede movel, etc...
     * @param leitura_humidade Especifica o valor da humidade lida, seja este ficticio ou real, isto é, gerado para
     *                         o Simulador ou obtido do Arduino.
     * @see Sensor#Sensor(String, int, int, String)
     * @since 08-01-2018
     */
    protected Humidade(String novo_nome, int latitude,int longitude, String intercomm, int leitura_humidade)
    {
        super(novo_nome,latitude,longitude,intercomm);
        reading_humidade = leitura_humidade;
    }
    /**
     * <p>Função para gerar um valor aleatorio para a leitura da humidade entre o valor minimo e o valor maximo fornecidos
     * de forma a este ser utilizado pelo Simulador para quando não é possivel obter dados reais.</p>
     * @param valor_minimo Valor minimo de humidade em percentagem a gerar. Tem de ser superior ou igual a 0.
     * @param valor_maximo Valor maximo de humidade em percentagem a gerar. Tem de ser inferior ou igual a 100.
     * @return Retorna a percentagem gerada aleatoriamente entre a margem minima e maxima especificadas.
     * @since 08-01-2018
     */
    public static int gerar_percentagem_humidade_random(int valor_minimo, int valor_maximo)//0 a 100 em percentagem
    {
        if(valor_maximo<valor_minimo){int aux = valor_maximo; valor_maximo = valor_minimo; valor_minimo = aux;}
        if(valor_minimo < 0){valor_minimo = 0;}
        if(valor_maximo > 100){valor_maximo = 100;}
        Random percentagem = new Random();
        int percentagem_final = percentagem.nextInt(valor_maximo)  + valor_minimo;
        return percentagem_final;
    }

}
