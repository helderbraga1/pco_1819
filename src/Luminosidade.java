import java.util.Random;

public class Luminosidade extends Sensor
{
    protected double leitura_lux;

    Luminosidade(String novo_nome, int latitude, int longitude, String intercomm, double leitura_luz)
    {
    super(novo_nome,latitude,longitude,intercomm);
    leitura_lux = leitura_luz;
    }
    public static double gerar_lux_random(double valor_minimo, double valor_maximo)//Noite nublada: 0.00001 lux Luz solar: 107527 lux
    {
        if(valor_minimo < 0){valor_minimo = 0;}
        Random lux = new Random();
        double lux_final = lux.nextDouble() * (valor_maximo - valor_minimo) + valor_minimo;
        return lux_final;
    }
}
