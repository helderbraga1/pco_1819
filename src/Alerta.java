/**
 * <h1>Classe Alerta</h1>
 * <p>Classe responsavel por emitir alertas para as diversas ocorrencias detetadas na cidade,
 * por exemplo um cidadao pode detetar que a barragem esta muito cheia e efectuar um pedido ao
 * sistema para que seja esvaziada alguma agua, tambem o sistema atravezs dos sensores que contem
 * pode detetar que a humidade do solo em um dos jardins se encontra abaixo do necessario para as
 * plantas se manterem soudaveis e decidir ligar o sistema de rega. Esta é subclasse da Classe Subsistema.</p>
 * @see Subsistema
 * @author Hélder Filipe Mendonça de Medeiros Braga
 * @since 08-01-2018
 */
public class Alerta extends Subsistema
{
    protected Cidadao cidadao;
    protected String mensagem;

    /**
     * <p>Construtor de um alerta gerado pelo sistema automatizado da cidade, obtem a latitude e longitude da ocorrencia
     * de modo a saber onde deve actuar, gera tambem uma mensagem para auxiliar a autarquia a identificar qual a ocorrencia.
     * Utiliza o construtor da classe mãe como base.</p>
     * @param latitude Latitude da ocorrencia detetada.
     * @param longitude Longitude da ocorrencia detetada.
     * @see Subsistema#Subsistema(int, int)
     * @since 08-01-2018
     */
    protected Alerta(int latitude,int longitude, String m)
    {
        super(latitude,longitude); //Nota: tem de ser a primeira linha do construtor.
        mensagem = m;
    }

    /**
     * <p>Construtor de um alerta reportado por um dos cidadãos da cidade, utiliza parcialmente o construtor
     * da classe Subsistema</p>
     * @param latitude Contem a latitude da ocorrencia reportada pelo cidadão.
     * @param longitude Contem a longitude da ocorrencia reportada pelo cidadão.
     * @param m Contem o relatorio da ocorrencia reportado pelo cidadão.
     * @param c Contem o cidadão que reportou a ocorrencia.
     * @see Subsistema#Subsistema(int, int)
     * @since 08-01-2018
     */
    protected Alerta(int latitude, int longitude, String m, Cidadao c)
    {
        super(latitude,longitude); //Nota: Tem de ser a primeira linha do construtor
        mensagem = m;
        cidadao = c;
    }
}
