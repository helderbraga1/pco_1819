/**
 * <h1>Classe Administrador</h1>
 * <p>A Classe Administrador é a classe responsavel por conter os administradores do programa,
 * esta é subclasse da classe Utilizador herdando os atributos "username" e "password" da classe mãe.</p>
 * @author Hélder Filipe Mendonça de Medeiros Braga
 * @since 07-01-2018
 */
public class Administrador extends Utilizador
{
    /**
     * <p>Classe constutora da Classe Administrador, utiliza o construtor da superclasse Utilizador.</p>
     * @param u nome do utilizador a criar
     * @param p password do utilizador a criar
     * @see Utilizador#Utilizador(String, String)
     * @since 07-01-2018
     */
    protected Administrador(String u, String p)
    {
        super(u,p); //Nota: Tem de ser a primeira linha do construtor da subclasse
    }
}